#=
this is the main function you want to modify
it should return 3 strings: the question name
the text of the question, and some infos for the grader.
the name has to be non-empty but do not need to be unique
you can use latex but you need to escape backslashes and dollar signs.
in a tring use "$v" to insert the value of the variable v
then just press shift+enter
=#
function sgstring(n)
	if n>0
		return "+"*string(n)
	else
		return string(n)
	end
end
function myExo() 
	I=vcat(collect(-3:-1), collect(1:3))
	x=[rand(I)]
	t=rand(I)
	while -t in x
		t=rand(I)
	end
	push!(x,t)
	t=rand(I)
	while t in x
		t=rand(I)
	end
	push!(x,t)

    a=rand(2:3)
    name="Asymptotes"
    question="Déterminez, si elles existent, les asymptotes de la fonction suivante:
    \$\$ \\frac{ $a x^2 $(sgstring(-a*(x[1]+x[2]))) x $(sgstring(a*x[1]*x[2])) }{ x $(sgstring(-x[3])) }. \$\$
    "
    solution="verticale en $(x[3]) (pas de piège). oblique y=$a x  $(sgstring(-a*(x[1]+x[2])+a*x[3]))"
    return name,question,solution
end
###########################################################
###########################################################
global output="<?xml version=\"1.0\" encoding=\"UTF-8\"?> <quiz>"
for i in 1:50 #change this   
    name,question,solution=myExo()
    global output*="<question type=\"essay\">
    <name>
      <text>$name</text>
    </name>
    <questiontext format=\"html\">
      <text><![CDATA[$question
       ]]></text>
    </questiontext>
    <generalfeedback format=\"html\">
      <text></text>
    </generalfeedback>
    <defaultgrade>1.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
    <idnumber></idnumber>
    <responseformat>editorfilepicker</responseformat>
    <responserequired>0</responserequired>
    <responsefieldlines>10</responsefieldlines>
    <attachments>3</attachments>
    <attachmentsrequired>0</attachmentsrequired>
    <graderinfo format=\"html\">
      <text><![CDATA[<div class=\"output_subarea output_text output_result\">
<pre>$solution</pre>
</div>]]></text>
    </graderinfo>
    <responsetemplate format=\"html\">
      <text></text>
    </responsetemplate>
  </question>"
end
output*="</quiz>"
f=open("/home/adrien/moodle/etude.xml","w")
write(f,output)
close(f)
