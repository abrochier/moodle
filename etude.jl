#=
this is the main function you want to modify
it should return 3 strings: the question name
the text of the question, and some infos for the grader.
the name has to be non-empty but do not need to be unique
you can use latex but you need to escape backslashes and dollar signs.
in a tring use "$v" to insert the value of the variable v
then just press shift+enter
=#
function addnumber(n)
	if n==0
        return ""
    elseif n<0
        return string(n) 
	else
		return "+"*string(n)
	end
end

function printandwait(mystr)
    println(mystr)
    println("press a enter")
    readline()
end

using Plots

function myExo(a,b,d) 

   
    #x = 1:10; y = rand(10); # These are the plotting data
    #plot(x, y)
    #gr(show = true)
     
    #println("press a enter")
    #readline()
        
    # start with nice g(x) = (x^2+1)/x whose derivative
    # g'(x) = (x^2-1)/x^2
    # asymptote oblique y = x
    # extrema en (1, 2) et (-1, -2)
    
    
    # consider now f(x) = g(ax-b)+d with
    
    #a = rand(vcat(collect(-3:-2), collect(2:3)))
    #b = rand(vcat(collect(-3:-2), collect(2:3)))
    #d = rand([-1,0,1])
    
    
    
    # actual function: f(x) = (a^2x^2+(ad-2ab)x+b^2+1-bd)/(ax-b) 
    # f'(x) = ag'(ax-b) = a((ax-b)^2-1)/(ax-b)^2 = (a^3x^2-2a^2bx+ab^2-a)/(ax-b)^2
    # extrema en ((b+1)/a, d+2) et ((b-1)/a, d-2)
    
    name="Étude de fonction"
    question="Soit \\(f:{\\mathbb R}\\to {\\mathbb R}\\) la fonction définie par:
    \$\$ f(x)= \\frac{ $(a^2) x^2 $(addnumber(a*d-2*a*b)) x $(addnumber(b^2-b*d+1)) }{$a x $(addnumber(-b)) }. \$\$
<ol>
<li>  Déterminer toutes les asymptotes de f.</li>
<li>  Résoudre l'équation \\(f(x)=0\\) et dresser le tableau de signes de \\(f\\)</li>
<li>  Calculer la dérivée de \\(f\\) et dresser le tableau de variations de \\(f\\) en prenant soin de décrire la nature des extrema s'il y en a.</li>
<li>  La fonction \\(f\\) est-elle injective ? Surjective ? Bijective ?</li>
<li>  Dessiner le graphe de \\(f\\) en faisant figurer toutes les informations obtenues ci-dessus.</li>
</ol>
    "
    solution="
    1. verticale x=$(b//a), oblique y=$a x $(addnumber(-b+d)).
    2. aucune solution, Delta = $((-2*a*b+a*d)^2-4*a^2*(b^2-b*d+1))
    2. extrema en ($((b+1)//a), $(d+2)) et ($((b-1)//a), $(d-2))
    3. dérivée = ($(a^3) x^2$(addnumber(-2a^2*b))x $(addnumber(a*b^2-a)))/($a x-$b)^2
    4. ...
    5. ni surjective (0 n'a pas d'antécédent) ni injective
    "
    
    fff(x) = (a^2*x^2+(a*d-2*a*b)*x+b^2+1-b*d)/(a*x-b) 
    asyy(x) = a*x-b+d
    gr(show = true)
    plot(fff, -3,3,ylim=(-10,10))
    plot!(asyy, -3,3,ylim=(-10,10))
 
#    printandwait(question*solution)
    return name,question,solution
end
###########################################################
###########################################################
global output="<?xml version=\"1.0\" encoding=\"UTF-8\"?> <quiz>"
for a in vcat(collect(-3:-2), collect(2:3)), 
b in      vcat(collect(-3:-2), collect(2:3)),
d in    [-1,0,1]
    name,question,solution=myExo(a,b,d)
    global output*="<question type=\"essay\">
    <name>
      <text>$name</text>
    </name>
    <questiontext format=\"html\">
      <text><![CDATA[$question
       ]]></text>
    </questiontext>
    <generalfeedback format=\"html\">
      <text></text>
    </generalfeedback>
    <defaultgrade>1.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
    <idnumber></idnumber>
    <responseformat>editorfilepicker</responseformat>
    <responserequired>0</responserequired>
    <responsefieldlines>10</responsefieldlines>
    <attachments>3</attachments>
    <attachmentsrequired>0</attachmentsrequired>
    <graderinfo format=\"html\">
      <text><![CDATA[<div class=\"output_subarea output_text output_result\">
<pre>$solution</pre>
</div>]]></text>
    </graderinfo>
    <responsetemplate format=\"html\">
      <text></text>
    </responsetemplate>
  </question>"
end
output*="</quiz>"
f=open("/home/adrien/moodle/etude.xml","w")
write(f,output)
close(f)
