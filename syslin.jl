#=
this is the main function you want to modify
it should return 3 strings: the question name
the text of the question, and some infos for the grader.
the name has to be non-empty but do not need to be unique
you can use latex but you need to escape backslashes and dollar signs.
in a tring use "$v" to insert the value of the variable v
then just press shift+enter
=#
function addnumber(n)
    s = string(n)
    if !(s[1] in ['-', '+'])
        s = "+"*s
    end
    
    return replace(s, "*" => "")
end

function printandwait(mystr)
    println(mystr)
    println("press a enter")
    readline()
end

function prettypolynomial(pol)
    if pol[0]*pol[1]==0
        return addnumber(pol)
    else
        return "+($(pol[0])$(addnumber(pol[1]))a)"
    end
end

#using Plots
using Polynomials


function randnzero()
   return rand([-3,-2,-1,1,2,3]) 
end
    
function prettyprint(tab)
    txt = ""
    for line in tab
        txt*="$(line[1])x$(prettypolynomial(line[2]))y$(prettypolynomial(line[3]))z=$(line[4])\n"
    end
    return txt
end

function myExo() 

   
    #x = 1:10; y = rand(10); # These are the plotting data
    #plot(x, y)
    #gr(show = true)
     
    #println("press a enter")
    #readline()
        
    # start with nice g(x) = (x^2+1)/x whose derivative
    # g'(x) = (x^2-1)/x^2
    # asymptote oblique y = x
    # extrema en (1, 2) et (-1, -2)
    
    
    # consider now f(x) = g(ax-b)+d with
    
    # Système après résolution:
    # x = 1
    #
    #
    
    sol = [0,0,0]
    sol[1] =rand(collect(-3:3)) 
    sol[2] =rand(collect(-3:3)) 
    speciala = rand(collect(-3:3)) 
    
    L1 = [1,0,0,sol[1]]
    L2 = [0,1,1,sol[2]]
    L3 = [0,0,Polynomial([-speciala,1], :a),sol[3]]
    systsimplifie= prettyprint([L1,L2,L3])   
    
    L1 = randnzero()*L1 + randnzero()*L2
    L2 = randnzero()*L1 + randnzero()*L2 + randnzero()*L3
    L3 = randnzero()*L1 + randnzero()*L3
    
            
    a = rand(vcat(collect(-3:-2), collect(2:3)))
    b = rand(vcat(collect(-3:-2), collect(2:3)))
    d = rand([-1,0,1])
    
    
    
    # actual function: f(x) = (a^2x^2+(ad-2ab)x+b^2+1-bd)/(ax-b) 
    # f'(x) = ag'(ax-b) = a((ax-b)^2-1)/(ax-b)^2 = (a^3x^2-2a^2bx+ab^2-a)/(ax-b)^2
    # extrema en ((b+1)/a, d+2) et ((b-1)/a, d-2)
    
    name="Système linéaire avec paramètre"
    question="Donner les solutions du système linéaire suivant, en fonction du paramètre réel \\(a\\):
$(prettyprint([L1,L2,L3]))    "
    solution="
    
    système après simplification:
$(systsimplifie)

    solution:
si a ≠ $(speciala), [x,y,z] = $(sol)
si a = $(speciala), [x,y,z] = [$(sol[1]), $(sol[2])-z, z], z libre
    "
    
    fff(x) = (a^2*x^2+(a*d-2*a*b)*x+b^2+1-b*d)/(a*x-b) 
    asyy(x) = a*x-b+d
    #gr(show = true)
    #plot(fff, -3,3,ylim=(-10,10))
    #plot!(asyy, -3,3,ylim=(-10,10))
 
    printandwait(question*solution)
    return name,question,solution
end
###########################################################
###########################################################
global output="<?xml version=\"1.0\" encoding=\"UTF-8\"?> <quiz>"
for i in 1:50 #change this   
    name,question,solution=myExo()
    global output*="<question type=\"essay\">
    <name>
      <text>$name</text>
    </name>
    <questiontext format=\"html\">
      <text><![CDATA[$question
       ]]></text>
    </questiontext>
    <generalfeedback format=\"html\">
      <text></text>
    </generalfeedback>
    <defaultgrade>1.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
    <idnumber></idnumber>
    <responseformat>editorfilepicker</responseformat>
    <responserequired>0</responserequired>
    <responsefieldlines>10</responsefieldlines>
    <attachments>10</attachments>
    <attachmentsrequired>0</attachmentsrequired>
    <graderinfo format=\"html\">
      <text><![CDATA[<div class=\"output_subarea output_text output_result\">
<pre>$solution</pre>
</div>]]></text>
    </graderinfo>
    <responsetemplate format=\"html\">
      <text></text>
    </responsetemplate>
  </question>"
end
output*="</quiz>"
f=open("tmp.xml","w")
write(f,output)
close(f)
